#include <stdio.h>
int main()
{
    char vow,*v;
    printf("Enter the character\n");
    vow=getchar();
    v=&vow;
    if(*v=='a' || *v=='e' || *v=='i' || *v=='o' || *v=='u')
        printf("Entered character is a vowel\n");
    else if(*v=='A' || *v=='E' || *v=='I' || *v=='O' || *v=='U')
        printf("Entered character is a vowel\n");
    else
        printf("Entered character is not a vowel\n");

    return 0;
}



#include <stdio.h>
void swap(int *a,int *b);
int main()
 {
  int num1,num2;
  printf("\n Enter the First Number");
  scanf("%d",&num1);
  printf("\n Enter the Second Number");
  scanf("%d",&num2);
  printf("Before Swapping the two numbers are %dand%d\n",num1,num2);
  swap(&num1,&num2);
  printf("After Swapping the two numbers are %dand%d\n",num1,num2);
  return 0;
 } 
 void swap( int *a, int *b)
 { 
  int temp;
  temp=*a;
  *a=*b;
  *b=temp;
 }
