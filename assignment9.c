#include <stdio.h>
int main()
{
    int i,j,k;
    int rows1,cols1,rows2,cols2;
    int mat1[3][3],mat2[3][3],res[5][5];
    printf("\n Enter the Number of Rows in First Matrix:");
    scanf("%d",&rows1);
    printf("\nEnter the Number of columns in First Matrix:");
    scanf("%d",&cols1);
    printf("\n Enter the Number of Rows in Second Matrix:");
    scanf("%d",&rows2);
    printf("\nEnter the Number of columns in Second Matrix:");
    scanf("%d",&cols2);
    if(cols1==rows2)
    {     
    int res_rows=rows1;
    int res_cols=cols1;
    printf("\nEnter the elements of First Matrix");
    for(i=0;i<rows1;i++)
    {
        for(j=0;j<cols1;j++)
            scanf("%d",&mat1[i][j]);
    }
 
    printf("\nEnter the elements of Second Matrix");
    for(i=0;i<rows2;i++)
    {
        for(j=0;j<cols2;j++)
            scanf("%d",&mat2[i][j]);
    }
 
    for(i=0;i<res_rows;i++)
    {
        j=0;
        for(j=0;j<res_cols;j++)
        {
            res[i][j]=0;
 
            for(k=0;k<res_cols;k++)
                res[i][j]+=mat1[i][k] * mat2[k][j];
        }
    }
 
    printf("The Elements of the Product Matrix are ");
 
    for(i=0;i<res_rows;i++)
    {
        printf("\n"); 
        for(j=0;j<res_cols;j++)
            printf("\t%d",res[i][j]);
    }
 
    return 0;
    }
    else
    {
        printf("\nThe Number of columns in the First matrix must be equal to the number of rows in the second matrix");
    }
}
