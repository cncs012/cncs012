#include <stdio.h>
#include<stdlib.h>
int main()
{  
    FILE *c1,*c2;
    char ch,filename1[20],filename2[20];
    printf("Enter the file name to copy from\n");
    gets(filename1);
    c1=fopen(filename1,"r");
    if(c1==NULL)
    {
        printf("Could not open file\n");
        exit(1);    
    }
    printf("Enter the name of file to paste in\n");
    gets(filename2);
    c2=fopen(filename2,"w");
    while(1)
    {
        ch=fgetc(c1);
        if(ch==EOF)
            break;
        else
            fputc(ch,c2);
    }
    fclose(c1);
    fclose(c2);

    return 0;
}

