#include <stdio.h>
int main()
{
    FILE *fp;
    char ch,txt[100];
    fp=fopen("INPUT.txt","w");
    if(fp==NULL)
    {
        printf("The file could not be opened\n");
        exit(1);
    }
    printf("Enter the text\n");
    gets(txt);
    fputs(txt,fp);
    fp=fopen("INPUT.txt","r");
    if(fp==NULL)
    {
        printf("Error opening file\n");
        exit(1);
    }
    ch=fgetc(fp);
    while(ch!=EOF)
    {
        putchar(ch);
        ch=fgetc(fp);
    }
    
    fclose(fp);
    return 0;
}
