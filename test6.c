#include <stdio.h>
int main()
{
    int i,j,mat[3][3];
    printf("Enter the values of matrix:\n");
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
            scanf("%d",&mat[i][j]);
    }
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
            printf("%d\t",mat[i][j]);
            printf("\n");
    }
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            mat[i][j]=mat[j][i];
        }    
    }
    printf("After transposing:\n");
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
            printf("%d\t",mat[i][j]);
            printf("\n");
    }
    
 
    return 0;
}
